```
#define cj about:cangjie
#define cj:uf(x...) cj(UserForum,x)
```

##### cj:uf(issues)

- [#242](https://gitcode.com/Cangjie/UsersForum/issues/242) -
Template literals
- [#266](https://gitcode.com/Cangjie/UsersForum/issues/266) -
`operator func` as args
- [#344](https://gitcode.com/Cangjie/UsersForum/issues/344) - Flow
- [#670](https://gitcode.com/Cangjie/UsersForum/issues/670) -` yield`? Actor?
- 

##### cj:uf(issues, comments)

/ built-in
 - [#667](https://gitcode.com/Cangjie/UsersForum/issues/667#tid-470861) -
 `extend Int64<: Addable<Int64>`
 - [#225](https://gitcode.com/Cangjie/UsersForum/issues/225) - `Atomic<Int>`

/ IDE/View
 - [#655](https://gitcode.com/Cangjie/UsersForum/issues/655?did=6b519de04c7b5f880e151e1d6a82b49db6e325a5#tid-1433897) -
  <del>lock/unlock</del>
 - [#700](https://gitcode.com/Cangjie/UsersForum/issues/700?did=fc18d97d74e2ede0a47300bdd8bee016e6f6ba26#tid-1432620) -
 nerd font,`<:`→`⊢`(`:-`) 
 - [#462](https://gitcode.com/Cangjie/UsersForum/issues/462?did=402515ec8389a8993e9238a2c3f020735525123a#tid-1432626) -
{by:`<-`,be:`=>`, to:`|>`,tobe:`~>`, on:`<:`, over:`<>`, at=`[]`}
 - [#474](https://gitcode.com/Cangjie/UsersForum/issues/474?did=bbb452ca4e01e26e7bb0deec1da5f3b821bd569f#tid-888673) -
 `if-let where`
 - [#627](https://gitcode.com/Cangjie/UsersForum/issues/627?did=12383d79ddc910be3962c389ef6fe185fb156e85#tid-1432326) -
 `for-iterator` lock